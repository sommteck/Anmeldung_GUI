using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anmeldung_GUI
{
    public partial class Benutzeranmeldung : Form
    {
        int zaehler = 0;

        public Benutzeranmeldung()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_input_Click(object sender, EventArgs e)
        {
            string PASSWORD = "Passwort#1", username;
            bool ok = false;

            username = txt_username.Text;

            if (zaehler < 3)
            {
                ok = false;
                if (txt_password.Text != PASSWORD)
                {
                    zaehler++;
                    MessageBox.Show("Falsches Passwort");
                    txt_password.Clear();
                    txt_password.Focus();
                }
                else
                {
                    MessageBox.Show("Korrecktes Passwort");
                    ok = true;
                    zaehler = 3;
                    cmd_show.Visible = lbl_output.Visible = numericUpDown1.Visible = true;
                }
            }
            if (ok == false && zaehler >= 3)
                Application.Exit();
        }

        private void cmd_show_Click(object sender, EventArgs e)
        {
            lbl_output.Text = "Angezeigter Wert: " + numericUpDown1.Value.ToString();
        }
    }
}
