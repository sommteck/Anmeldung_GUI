namespace Anmeldung_GUI
{
    partial class Benutzeranmeldung
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.cmd_input = new System.Windows.Forms.Button();
            this.cmd_show = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.lbl_output = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Benutzername:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Passwort:";
            // 
            // txt_username
            // 
            this.txt_username.Location = new System.Drawing.Point(177, 44);
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(100, 22);
            this.txt_username.TabIndex = 2;
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(177, 106);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(100, 22);
            this.txt_password.TabIndex = 3;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(46, 175);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Visible = false;
            // 
            // cmd_input
            // 
            this.cmd_input.Location = new System.Drawing.Point(46, 238);
            this.cmd_input.Name = "cmd_input";
            this.cmd_input.Size = new System.Drawing.Size(75, 23);
            this.cmd_input.TabIndex = 5;
            this.cmd_input.Text = "&OK";
            this.cmd_input.UseVisualStyleBackColor = true;
            this.cmd_input.Click += new System.EventHandler(this.cmd_input_Click);
            // 
            // cmd_show
            // 
            this.cmd_show.Location = new System.Drawing.Point(163, 238);
            this.cmd_show.Name = "cmd_show";
            this.cmd_show.Size = new System.Drawing.Size(75, 23);
            this.cmd_show.TabIndex = 6;
            this.cmd_show.Text = "&Anzeige";
            this.cmd_show.UseVisualStyleBackColor = true;
            this.cmd_show.Visible = false;
            this.cmd_show.Click += new System.EventHandler(this.cmd_show_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(291, 238);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 23);
            this.cmd_end.TabIndex = 7;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(202, 175);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(45, 16);
            this.lbl_output.TabIndex = 8;
            this.lbl_output.Text = "label3";
            this.lbl_output.Visible = false;
            // 
            // Benutzeranmeldung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 322);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_show);
            this.Controls.Add(this.cmd_input);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.txt_password);
            this.Controls.Add(this.txt_username);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Benutzeranmeldung";
            this.Text = "Benutzeranmeldung";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button cmd_input;
        private System.Windows.Forms.Button cmd_show;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Label lbl_output;
    }
}

